const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes");

const app = express();


app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));


mongoose.set('strictQuery',true);

mongoose.connect("mongodb+srv://admin:admin123@clusterbatch248.y70nxyz.mongodb.net/booking-api?retryWrites=true&w=majority",
    {
        useNewUrlParser:true,
        useUnifiedTopology: true
    })

let db = mongoose.connection;

db.on("error",console.error.bind(console,"Connction error"));
db.once("open",()=>console.log("We're connected to MongoDB Atlas!"));


app.use("/users",userRoutes);
app.use("/courses",courseRoutes);


app.listen(process.env.PORT || 4000, ()=>{
    
    console.log(`ExpressJS API is now online on port ${process.env.PORT || 4000}`);

})