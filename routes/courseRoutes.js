const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseControllers");
const auth = require("../auth");



router.post("/",auth.verify,(req,res)=>{

	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	courseController.addCourse(data).then(resultFromController=>res.send(resultFromController))

})

router.get("/all",(req,res)=>{

	courseController.getAllCourse().then(resultFromController=>res.send(resultFromController))
})


router.get("/",(req,res)=>{

	courseController.getAllActive().then(resultFromController=>res.send(resultFromController))

})


router.put("/:courseId",auth.verify,(req,res)=>{

	courseController.updateCourse(req.params,req.body).then(resultFromController=>res.send(resultFromController))
})


router.patch("/:courseId/archive",auth.verify,(req,res)=>{

	courseController.archiveCourse(req.params,req.body).then(resultFromController=>res.send(resultFromController))
})

router.patch("/:courseId/unarchive",auth.verify,(req,res)=>{

	courseController.unarchiveCourse(req.params,req.body).then(resultFromController=>res.send(resultFromController))
})



module.exports = router;
