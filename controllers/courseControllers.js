const Course = require("../models/Course");



module.exports.addCourse = (data) =>{

	if(data.isAdmin){

		let newCourse = new Course({

				name: data.course.name,
				description: data.course.description,
				price: data.course.price
				
			});

			return newCourse.save().then((course,error)=>{
				if(error){
					return false;
				}
				else {
					return true;
				}
			})


	}

	else {
					
		return false;
				
		}
	
}


module.exports.getAllCourse = () =>{

	return Course.find({}).then(result=>result);
}

module.exports.getAllActive = () =>{

	return Course.find({isActive: true}).then(result=>result);
}

module.exports.updateCourse = (reqParams,reqBody) =>{

	let updateCourse = {
		name:reqBody.name,
		description:reqBody.description,
		price:reqBody.price
	}

	return Course.findByIdAndUpdate(reqParams.courseId,updateCourse).then((course,error)=>{

		return error?false:true;


	})
}



module.exports.archiveCourse = (reqParams,reqBody) =>{
		

	     
		return Course.findById(reqParams.courseId).then(result=>{

			
			    result.isActive = reqBody?false:reqBody.isActive;	

				return result.save().then((result,error)=>{

					return error?false:true;
				});
			
			
		})

}


module.exports.unarchiveCourse = (reqParams,reqBody) =>{
		
	return Course.findById(reqParams.courseId).then(result=>{

			
			result.isActive = reqBody?true:reqBody.isActive;	

			return result.save().then((result,error)=>{

				return error?false :true;
			});
		

	})

}

